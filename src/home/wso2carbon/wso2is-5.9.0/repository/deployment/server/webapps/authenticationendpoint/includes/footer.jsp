<%--
  ~ Copyright (c) 2019, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
  ~
  ~ WSO2 Inc. licenses this file to you under the Apache License,
  ~ Version 2.0 (the "License"); you may not use this file except
  ~ in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  --%>

<!-- localize.jsp MUST already be included in the calling script -->
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.AuthenticationEndpointUtil" %>

<!-- footer -->
<footer class="footer">
    <div class="container-fluid">
      
        <p>  &copy; <script>document.write(new Date().getFullYear());</script> 
            <%=AuthenticationEndpointUtil.i18n(resourceBundle, "wso2.identity.server")%> |
            <!-- 
         <a href="<%=AuthenticationEndpointUtil.i18n(resourceBundle, "business.homepage")%>"
               target="_blank">
               <%=AuthenticationEndpointUtil.i18n(resourceBundle, "all.rights.reserved")%>
               
            </a>
            -->
            <a href="https://www.renzhengyide.com/?page_id=109"
                    target="_blank">
                    <%=AuthenticationEndpointUtil.i18n(resourceBundle, "all.rights.reserved")%>
                    
                 </a>
        </p>
    </div>
</footer>
